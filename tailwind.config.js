module.exports = {
    purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            height: {
                max: 'max-content',
            },
            minHeight: {
                300: '300px',
            },
        },
    },
    variants: {
        extend: {
            borderWidth: ['focus', 'focus-within'],
            height: ['responsive', 'hover', 'focus'],
        },
    },
    plugins: [],
};
