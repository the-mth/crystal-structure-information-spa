import { AxiosRequestConfig } from 'axios';
import { IGetCrystalStructuresResponse } from '../../interfaces/Api';
import { apiGet } from '../../services/Http';

const crystalStructuresUrl = 'dois';

export function apiGetCrystalStructures(
    onSuccess: ((content: IGetCrystalStructuresResponse) => any) | null,
    onError: ((error: any) => any) | undefined
) {
    const config: AxiosRequestConfig = {
        params: {
            query: 'prefix:10.5517',
        },
    };
    return apiGet<IGetCrystalStructuresResponse>(crystalStructuresUrl, config)
        .then((res) => onSuccess && onSuccess(res.data))
        .catch((err) => onError && onError(err));
}
