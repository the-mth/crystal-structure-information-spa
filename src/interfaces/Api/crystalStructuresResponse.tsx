import { ICrystalStructure } from '../CrystalStructure';

export interface IGetCrystalStructuresResponse {
    data: ICrystalStructure[];
    links: any;
    meta: any;
}
