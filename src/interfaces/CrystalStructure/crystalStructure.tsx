export interface ICrystalStructure {
    id: string;
    type: any;
    attributes: any;
    relationships: any;
}
