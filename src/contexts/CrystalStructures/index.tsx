import { createContext, useContext, useEffect, useState } from 'react';
import { apiGetCrystalStructures } from '../../actions/Api';
import { ICrystalStructure } from '../../interfaces/CrystalStructure';
const CrystalStructuresDataContext = createContext(null);

interface ICrystalStructuresDataContext {
    isLoading: boolean;
    crystalStructures: ICrystalStructure[];
    selectedCrystalStructure: ICrystalStructure;
    setSelectedCrystalStructure: (selected: ICrystalStructure) => any;
}

export function CrystalStructureDataProvider(props: any) {
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [crystalStructures, setCrystalStuctures] = useState<ICrystalStructure[]>();
    const [selectedCrystalStructure, setSelectedCrystalStructure] = useState<ICrystalStructure>();

    const fetchData = () => {
        setIsLoading(true);
        apiGetCrystalStructures(
            (content) => {
                setCrystalStuctures(content.data);
                setIsLoading(false);
            },
            (err) => {
                setIsLoading(false);
            }
        );
    };

    useEffect(() => {
        if (!crystalStructures && !isLoading) {
            fetchData();
        }
    }, [crystalStructures, isLoading]);

    const value = {
        isLoading,
        setIsLoading,
        crystalStructures,
        setCrystalStuctures,
        selectedCrystalStructure,
        setSelectedCrystalStructure,
    };

    return <CrystalStructuresDataContext.Provider value={value} {...props} />;
}

export function useCrystalStructuresDataContext(): ICrystalStructuresDataContext {
    const context = useContext(CrystalStructuresDataContext);
    if (!context) {
        throw new Error('useCrystalStructuresDataContext must be used within CrystalStructuresDataContext');
    }
    return context;
}
