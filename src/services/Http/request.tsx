import axios, { AxiosRequestConfig } from 'axios';

export async function apiGet<T>(url: string, config: AxiosRequestConfig) {
    return await axios.get<T>(process.env.REACT_APP_API_ENDPOINT + url, config);
}
