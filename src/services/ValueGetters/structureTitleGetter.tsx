import { ICrystalStructure } from '../../interfaces/CrystalStructure';

export function structureTitleGetter(structure: ICrystalStructure) {
    const name = !!structure?.attributes?.titles ? structure?.attributes?.titles[0]?.title : '-';
    return name;
}
