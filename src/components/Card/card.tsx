export function Card({ className = '', children, title }: any) {
    return (
        <div className={`border p-8 flex flex-col shadow-md ${className}`}>
            <div className="pb-8">
                <CardTitle>{title}</CardTitle>
            </div>
            <div className="flex-1 flex flex-col">{children}</div>
        </div>
    );
}

export function CardTitle({ className = '', children }: any) {
    return <h1 className={className}>{children}</h1>;
}
