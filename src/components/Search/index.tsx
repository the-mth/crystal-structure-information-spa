import searchIcon from '../../assets/search.svg';

export function SearchInput({ className = '', placeholder = 'Search', ...rest }) {
    const inputClassNames = `flex-1 px-2 py-2 focus:outline-none ${className}`;

    return (
        <div className="flex border-b border-gray-400 mb-4 focus-within:border-b-2">
            <img className="w-3" alt="input icon" src={searchIcon} />
            <input className={inputClassNames} aria-label={placeholder} placeholder={placeholder} {...rest} />
        </div>
    );
}
