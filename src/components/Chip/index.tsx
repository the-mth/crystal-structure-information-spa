export function Chip({ className = '', children }: any) {
    return <div className={'px-6 py-1 rounded-3xl border mr-2 mb-2 text-gray-400 ' + className}>{children}</div>;
}
