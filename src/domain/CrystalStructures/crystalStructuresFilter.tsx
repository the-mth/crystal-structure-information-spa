import { useState } from 'react';
import { SearchInput } from '../../components/Search';

function CrystalStructuresFilter({ children }: any) {
    const [filterValue, setFiltervalue] = useState<string>('');
    return (
        <>
            <SearchInput
                placeholder="Search Crystal Structures"
                onChange={(e: any) => {
                    setFiltervalue(e.target.value);
                }}
            />
            {children(filterValue)}
        </>
    );
}

export default CrystalStructuresFilter;
