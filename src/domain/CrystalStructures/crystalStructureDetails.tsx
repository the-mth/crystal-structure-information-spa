import { Chip } from '../../components/Chip';
import { useCrystalStructuresDataContext } from '../../contexts/CrystalStructures';
import { structureTitleGetter } from '../../services/ValueGetters/structureTitleGetter';

export function CrystalStructureDetails({ className = '' }) {
    const { selectedCrystalStructure } = useCrystalStructuresDataContext();

    return (
        <div className={className}>
            {!selectedCrystalStructure ? (
                <>
                    <h2>Selected Crystal Structure</h2>
                    <div className="text-center text-sm">Select a Crystal Structure to view more details.</div>
                </>
            ) : (
                <div>
                    <CrystalStructureDisplayValue label="Id">
                        {selectedCrystalStructure.id}
                    </CrystalStructureDisplayValue>
                    <CrystalStructureDisplayValue label="Title">
                        {structureTitleGetter(selectedCrystalStructure)}
                    </CrystalStructureDisplayValue>
                    <CrystalStructureDisplayValue label="Publisher">
                        {selectedCrystalStructure.attributes.publisher}
                    </CrystalStructureDisplayValue>
                    <CrystalStructureDisplayValue label="Publication Year">
                        {selectedCrystalStructure.attributes.publicationYear}
                    </CrystalStructureDisplayValue>
                    <CrystalStructureDisplayValue label="Subjects">
                        <div className="flex flex-wrap pb-2">
                            {selectedCrystalStructure.attributes.subjects.map((s: any, i: number) => (
                                <Chip className="mr-2 mb-2" key={i}>
                                    <p>{s.subject}</p>
                                </Chip>
                            ))}
                        </div>
                    </CrystalStructureDisplayValue>
                    <CrystalStructureDisplayValue label="URL">
                        <div className="flex">
                            <a
                                href={selectedCrystalStructure.attributes.url}
                                target="_blank"
                                rel="noreferrer"
                                title={structureTitleGetter(selectedCrystalStructure)}
                            >
                                open on website
                            </a>
                        </div>
                    </CrystalStructureDisplayValue>
                </div>
            )}
        </div>
    );
}

function CrystalStructureDisplayValue({ label = '', children }: any) {
    return (
        <div className="md:flex text-sm py-1">
            <label htmlFor={label} className="w-40">
                {label}:
            </label>
            <h3 id={label} className="flex-1 font-semibold">
                {children}
            </h3>
        </div>
    );
}
