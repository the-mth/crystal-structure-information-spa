import CrystalStructuresCard from './crystalStructuresCard';

function CrystalStructuresPage() {
    return (
        <div className="flex-1 flex py-8 sm:px-8">
            <CrystalStructuresCard />
        </div>
    );
}
export default CrystalStructuresPage;
