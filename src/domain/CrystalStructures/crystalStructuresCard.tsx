import { Card } from '../../components/Card';
import { CrystalStructureDetails } from './crystalStructureDetails';
import CrystalStructuresFilter from './crystalStructuresFilter';
import CrystalStructuresTable from './crystalStructuresTable';

function CrystalStructuresCard() {
    return (
        <Card title="Crystal Structures" className="flex-grow">
            <CrystalStructuresFilter>
                {(filterValue: string) => <CrystalStructuresTable filter={filterValue} />}
            </CrystalStructuresFilter>
            <CrystalStructureDetails className="pt-6 border-t" />
        </Card>
    );
}

export default CrystalStructuresCard;
