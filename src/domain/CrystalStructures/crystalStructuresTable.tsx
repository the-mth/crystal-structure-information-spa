import { AgGridColumn } from 'ag-grid-react';
import { AgGridReact } from 'ag-grid-react';
import { useCrystalStructuresDataContext } from '../../contexts/CrystalStructures';
import { structureTitleGetter } from '../../services/ValueGetters/structureTitleGetter';

function CrystalStructuresTable({ filter = '' }) {
    const { crystalStructures, setSelectedCrystalStructure } = useCrystalStructuresDataContext();

    let data = crystalStructures;
    if (crystalStructures) {
        data = crystalStructures?.filter(
            (s) => s.id.indexOf(filter) >= 0 || structureTitleGetter(s).toLocaleLowerCase().indexOf(filter) >= 0
        );
    }

    const nameValueGetter = (props: any) => {
        return structureTitleGetter(props.data);
    };

    return (
        <div className="ag-theme-alpine w-100 flex-1 min-h-300">
            <AgGridReact
                defaultColDef={{
                    sortable: true,
                    filter: true,
                    minWidth: 250,
                }}
                onSelectionChanged={(props: any) => {
                    const selected = props.api.getSelectedRows();
                    setSelectedCrystalStructure(selected[0]);
                }}
                rowData={data}
                rowSelection="single"
            >
                <AgGridColumn headerName="Name" flex={3} valueGetter={nameValueGetter}></AgGridColumn>
                <AgGridColumn field="id" flex={1}></AgGridColumn>
            </AgGridReact>
        </div>
    );
}

export default CrystalStructuresTable;
