import { CrystalStructureDataProvider } from '../../contexts/CrystalStructures';
import CrystalStructuresPage from '../CrystalStructures/crystalStructuresPage';

function App() {
    return (
        <div className="min-h-screen flex flex-col">
            <CrystalStructureDataProvider>
                <CrystalStructuresPage />
            </CrystalStructureDataProvider>
        </div>
    );
}
export default App;
